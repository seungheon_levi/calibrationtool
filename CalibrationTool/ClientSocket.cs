﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace CalibrationTool
{

    class ClientSocket
    {
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern IntPtr FindWindow(string strClassName, string strWindowName);

        [DllImport("user32.dll")]
        public static extern int SendMessage(int hWnd, uint Msg, int wParam, int lParam);

        public const int WM_USER = 0x0400;
        public const int WM_SOCKET_CLOSE = WM_USER + 10;
        public const int WM_SOCKET_RECEIVE = WM_USER + 11;

        TcpClient socket = null;
        string IPAddress;
        int portNumber;

        ClientSocket(string _IPAddress, int _portNumber)
        {
            socket = new TcpClient();
            IPAddress = _IPAddress;
            portNumber = _portNumber;
        }

        ~ClientSocket()
        {

        }

        bool LogFileMsg(String logMsg)
        {
            string currentPath = null;
            currentPath = AppDomain.CurrentDomain.BaseDirectory;

            string logFile = currentPath + "\\Log\\log.dat";

            FileStream fs = File.Open(logFile, FileMode.OpenOrCreate);

            fs.Seek(-1, SeekOrigin.End);

            string finalMsg = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "  " + logMsg;

            fs.Write(Encoding.UTF8.GetBytes(finalMsg), 0, finalMsg.Length);

            fs.Close();

            return true;
        }

        void SetWnd(int hWnd)
        {
            m_phWnd = hWnd;
        }

	    void OnClose(int nErrorCode)
        {
            string strerr = String.Format("Socket Error code : %d", nErrorCode);
            LogFileMsg(strerr);

            SendMessage(m_phWnd, WM_SOCKET_CLOSE, 0, 0);
            socket.Close();
        }
        void OnConnect(int nErrorCode)
        {
            string strerr = String.Format("Socket Error code : %d", nErrorCode);
            LogFileMsg(strerr);

            SendMessage(m_phWnd, WM_SOCKET_RECEIVE, 0, 0);
            socket.Connect(IPAddress, portNumber);
        }
        void OnReceive(int nErrorCode)
        {
            string strerr = String.Format("Socket Error code : %d", nErrorCode);
            LogFileMsg(strerr);

           // socket.Connect(nErrorCode);
        }

        int m_phWnd;
    }
}
