﻿using Microsoft.Win32;
using Microsoft.WindowsAPICodePack.Dialogs;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Point = System.Windows.Point;

namespace CalibrationTool
{
    /// <summary>
    /// MainWindow.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class MainWindow : Window
    {
        SettingData settingData;
        DataController dataController;

        int windowValue; // 0 ~ 65535 width  
        int levelValue;  // 0 ~ 65535 center 32768
        int pixelValue;
        float zoomFactor; // 0.264f, 0.5f, 0.75f, 1.0f, 1.25f

        Point downPosition;
        int mouseX;
        int mouseY;
        ushort[] showingImage;
        byte[] rgbDatas;
        bool moveMode = false;
        bool editMode = false;
        private Util util;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void CalcHistogram(ushort[] pixelValues, int width, int height)
        {
            int downSizing = 64;
            ushort[] bin = new ushort[ushort.MaxValue/ downSizing + 1];
            
            int size = width * height;
            for(int i = 0; i < size; i++)
            {
                int b = pixelValues[i]/ downSizing;
                bin[b]++;
            }
            int maxBin = -1;
            for (int b = 0; b < ushort.MaxValue/ downSizing; b++)
            {
                if (maxBin < bin[b]) maxBin = bin[b];
            }
            double rectWidth = calibrationHistogram.Width;
            double rectHeight = calibrationHistogram.Height;

            double ww = rectWidth / ushort.MaxValue * downSizing;
            double hh = rectHeight / (maxBin + 20000);
            
            Polyline polyline = new Polyline();
            polyline.Stroke = System.Windows.Media.Brushes.White;
            polyline.StrokeThickness = 1;
            

            System.Windows.Media.PointCollection pointCollection = new System.Windows.Media.PointCollection();
            System.Windows.Point p;
            
            p = new System.Windows.Point(0, rectHeight);
            pointCollection.Add(p);

            for (int b = 0; b < ushort.MaxValue/ downSizing; b++)
            {
                //if (bin[b] == 0) continue;
                //p = new System.Windows.Point(ww * b, rectHeight);
                //pointCollection.Add(p);
                p = new System.Windows.Point(ww * b, rectHeight - hh * bin[b]);
                pointCollection.Add(p);
            }

            p = new System.Windows.Point(rectWidth, rectHeight);
            pointCollection.Add(p);

            polyline.Points = pointCollection;
            
            calibrationHistogram.Children.Add(polyline);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            settingData = new SettingData();

            LoadSettingData();

            util = new Util(logListBox, settingData);

            dataController = new DataController(settingData, util, settingData.FrameWidth, settingData.FrameHeight);

            showingImage = new ushort[settingData.FrameWidth * settingData.FrameHeight];

            EditReset();
        }

       
        private void INT_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
           e.Handled = !IsInteger(e.Text);
        }

        private bool IsInteger(string text)
        {
            Regex regex = new Regex("[^0-9]"); ;

            return !regex.IsMatch(text);
        }

        private void FLOAT_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsNumeric(e.Text);
        }

        private bool IsNumeric(string text)
        {
            Regex regex = new Regex("[^0-9.-]"); ;

            return !regex.IsMatch(text);
        }

        private void GetImageButton_Click(object sender, RoutedEventArgs e)
        {
            Button getImageButton = (Button)sender;
            string fileName = "";
            if (getImageButton.Name.Equals("DarkImage"))
            {
                util.LogFormat("GetDarkImageButton_Click");
                //fileName = dataController.GetDarkImage();
                fileName = SaveRandomImage(0);
                if (!fileName.Equals(string.Empty))
                {
                    if (DarkImageListBox.Items.Count >= 5)
                    {
                        ImageDelete(0, DarkImageListBox.Items.GetItemAt(0).ToString());
                        DarkImageListBox.Items.RemoveAt(0);
                    }

                    DarkImageListBox.Items.Add(fileName);
                    DarkImageListBox.Focus();
                    DarkImageListBox.SelectedIndex = DarkImageListBox.Items.Count - 1;
                }
                else
                {
                    MessageBox.Show("Failed get Dark Image");
                }
            }
            else if (getImageButton.Name.Equals("BrightImage"))
            {
                util.LogFormat("GetBrightImageButton_Click");
                //fileName = dataController.GetBrightImage();
                fileName = SaveRandomImage(1);
                if (!fileName.Equals(string.Empty))
                {
                    if (BrightImageListBox.Items.Count >= 5)
                    {
                        ImageDelete(0, BrightImageListBox.Items.GetItemAt(0).ToString());
                        BrightImageListBox.Items.RemoveAt(0);
                    }

                    BrightImageListBox.Items.Add(fileName);
                    BrightImageListBox.Focus();
                    BrightImageListBox.SelectedIndex = BrightImageListBox.Items.Count - 1;
                   
                }
                else
                {
                    MessageBox.Show("Failed get Bright Image");
                }
            }
            else if(getImageButton.Name.Equals("AcqusitionImage"))
            {
                util.LogFormat("GetAcqusitionImageButton_Click");
                fileName = dataController.GetAcquisitionImage();
                if (!fileName.Equals(string.Empty))
                {
                    dataController.Calibrate(fileName);
                    AcqusitionImageListBox.Items.Add(fileName);
                    AcqusitionImageListBox.Focus();
                    AcqusitionImageListBox.SelectedIndex = AcqusitionImageListBox.Items.Count - 1;
                }
                else
                {
                    MessageBox.Show("Failed get Acquisition Image");
                }
            }
        }

        private void ImageDelete(int type, string fileName)
        {
            util.LogFormat(fileName + "Image Delete");
            string fullFileName = "";
            if (type == 0)//image
            {
                fullFileName = settingData.ImageFilePath + "\\" + fileName;
            }
            else // calibration image
            {
                fullFileName = settingData.CalibrationFilePath + "\\" + fileName;
            }
            if (File.Exists(fullFileName)) File.Delete(fullFileName);
        }

        private void ImageOpen(int type, string fileName, int width, int height)
        {
            int size = width * height;
            showingImage = new ushort[size];
            if(type == -1) // drop full path
            {
                FileStream fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
                for (int i = 0; i < size; i++)
                {
                    byte[] readPixel = new byte[2];
                    fileStream.Read(readPixel, 0, 2);

                    showingImage[i] = BitConverter.ToUInt16(readPixel, 0);
                }
                fileStream.Close();

                ImageRefresh(type, width, height);

                CalcHistogram(showingImage, width, height);
            }
            if(type == 0 || type == 1 || type == 2 || type == 3|| type == 5 || type == 6) // Dark,Bright,meanDark,offset, bpm, calibrated image
            {
                string fullFileName = "";
                if ( type == 0 || type == 1 || type == 6) // dark, bright, calibrated
                    fullFileName = settingData.ImageFilePath + "\\" + fileName;
                else
                    fullFileName = settingData.CalibrationFilePath + "\\" + fileName;

                FileStream fileStream = new FileStream(fullFileName, FileMode.Open, FileAccess.Read);
                for (int i = 0; i < size; i++)
                {
                    byte[] readPixel = new byte[2];
                    fileStream.Read(readPixel, 0, 2);

                    showingImage[i] = BitConverter.ToUInt16(readPixel, 0);
                }
                fileStream.Close();

                ImageRefresh(type, width, height);
                
                CalcHistogram(showingImage, width, height);
            }
            if(type == 4) // gain
            {
                string fullFileName = settingData.CalibrationFilePath + "\\" + fileName;

                FileStream fileStream = new FileStream(fullFileName, FileMode.Open, FileAccess.Read);
                float[] gainMapRaw = new float[size];

                float max = float.MinValue;
                float min = float.MaxValue;
                for (int i = 0; i < size; i++)
                {
                    byte[] readPixel = new byte[sizeof(float)];
                    fileStream.Read(readPixel, 0, sizeof(float));

                    gainMapRaw[i] = BitConverter.ToSingle(readPixel, 0);

                    if (gainMapRaw[i] > max) max = gainMapRaw[i];
                    if (gainMapRaw[i] < min) min = gainMapRaw[i];
                }
                fileStream.Close();

                float fSize = max - min;

                for(int i = 0; i < size; i++)
                {
                    showingImage[i] = (ushort)((gainMapRaw[i] * ushort.MaxValue) / fSize);
                }

                ImageRefresh(type, width, height);

                CalcHistogram(showingImage, width, height);
            }

           

        }

        private void ImageRefresh(int type, int width, int height)
        {
            Bitmap bmp;
            if (type == 5)
            {
                if(rgbDatas == null)
                    rgbDatas = Convert16BitGrayScaleToRGB(showingImage, width, height);
                bmp = CreateBitmapFromBytes(rgbDatas, width, height);
            }
            else
            {
                var rgbData = Convert16BitGrayScaleToRGB(showingImage, width, height, levelValue, windowValue);
                bmp = CreateBitmapFromBytes(rgbData, width, height);
            }

            // display bitmap
            int index = menuTabContrl.SelectedIndex;
            if (index == 1) // dark, meandark, bright, gain
            {
                calibrationImage.Source = BitmapToImageSource(bmp);
                calibrationNavigationImage.Source = BitmapToImageSource(bmp);
            }
            else if(index == 2) // acqu, meandark, offset, gain, bpm
            {
                acquistionImage.Source = BitmapToImageSource(bmp);
                acquistionNavigationImage.Source = BitmapToImageSource(bmp);
            }
            else if(index == 3)// bpm
            {
                defectCalibrationImage.Source = BitmapToImageSource(bmp);
                defectCalibrationNavigationImage.Source = BitmapToImageSource(bmp);
            }
        }

        private string SaveRandomImage(int type)
        {
            string fileName = "";

            Random r = new Random();
            int width = 3072;
            int height = 3072;
            int size = width * height;
            ushort[] pixelValues = new ushort[width * height];
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    pixelValues[y * width + x] = (ushort)r.Next(ushort.MaxValue / 8, ushort.MaxValue / 2);// (ushort)((x * 10) % 16383);
                }
            }

            DateTime dateTime = DateTime.Now;
            fileName = ((type == 0)? "dark_" : "bright_") + dateTime.ToString("yyyyMMddHHmmss") + ".raw";
            string fullFileName =  settingData.ImageFilePath + "\\" + fileName;
            
            FileStream fileStream = new FileStream(fullFileName, FileMode.Create | FileMode.Append, FileAccess.Write);
            for (int i = 0; i < size; i++)
            {
                fileStream.Write(BitConverter.GetBytes(pixelValues[i]), 0, 2);
            }

            fileStream.Close();

            return fileName;
        }

        private void calibrationFileButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog folderDialog = new OpenFileDialog() { InitialDirectory = calibrationFileTextBox.Text};
            folderDialog.ValidateNames = false;
            folderDialog.CheckFileExists = false;
            folderDialog.CheckPathExists = true;

            folderDialog.FileName = "Folder Selection";

            if (folderDialog.ShowDialog() == true) 
            {
                string[] dir = folderDialog.FileName.Split('\\');
                string loc = dir[0];
                for (int i = 1; i < dir.Length - 1; i++)
                    loc += ("\\" + dir[i]);
                calibrationFileTextBox.Text = loc;
                settingData.CalibrationFilePath = calibrationFileTextBox.Text;
            }

        }

        private void logFileButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog folderDialog = new OpenFileDialog() { InitialDirectory = logFileTextBox.Text };
            folderDialog.ValidateNames = false;
            folderDialog.CheckFileExists = false;
            folderDialog.CheckPathExists = true;

            folderDialog.FileName = "Folder Selection";

            if (folderDialog.ShowDialog() == true) 
            {
                string[] dir = folderDialog.FileName.Split('\\');
                string loc = dir[0];
                for (int i = 1; i < dir.Length - 1; i++)
                    loc += ("\\" + dir[i]);
                logFileTextBox.Text = loc;
                settingData.LogFilePath = logFileTextBox.Text;
                if (util != null) util = new Util(logListBox, settingData);
                    
            }
        }

        private void imageFileButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog folderDialog = new OpenFileDialog() { InitialDirectory = imageFileTextBox.Text };
            folderDialog.ValidateNames = false;
            folderDialog.CheckFileExists = false;
            folderDialog.CheckPathExists = true;

            folderDialog.FileName = "Folder Selection";

            if (folderDialog.ShowDialog() == true)
            {
                string[] dir = folderDialog.FileName.Split('\\');
                string loc = dir[0];
                for (int i = 1; i < dir.Length - 1; i++)
                    loc += ("\\" + dir[i]);
                imageFileTextBox.Text = loc;
                settingData.ImageFilePath = imageFileTextBox.Text;
            }
        }

        private void settingResetButton_Click(object sender, RoutedEventArgs e)
        {
            LoadSettingData();
        }

        private void settingApplyButton_Click(object sender, RoutedEventArgs e)
        {
            SaveSettingData();
            settingData.SaveSettingFile();
            dataController.ApplySettingData();
            
        }

        private void LoadSettingData()
        {
            /// network 
            string[] ips = settingData.IPs.Split('.');
            IP_1.Text = ips[0];
            IP_2.Text = ips[1];
            IP_3.Text = ips[2];
            IP_4.Text = ips[3];

            port.Text = settingData.Port.ToString();

            timeout.Text = settingData.Timeout.ToString();

            detectorModeAuto.IsChecked = settingData.DetectorModeAuto;
            detectorModeHandShaking.IsChecked = !settingData.DetectorModeAuto;

            sensitivity.Text = settingData.Sensitivity.ToString();
            triggerSensitivity.Text = settingData.TriggerSensitivity.ToString();

            framewidth.Text = settingData.FrameWidth.ToString();
            frameHeight.Text = settingData.FrameHeight.ToString();
            windowtime.Text = settingData.WindowTime.ToString();

            /// Location
            calibrationFileTextBox.Text = settingData.CalibrationFilePath;
            logFileTextBox.Text = settingData.LogFilePath;
            imageFileTextBox.Text = settingData.ImageFilePath;

            /// image Cut edge
            topTextBox.Text = settingData.TopCutEdge.ToString();
            bottomTextBox.Text = settingData.BottomCutEdge.ToString();
            leftTextBox.Text = settingData.LeftCutEdge.ToString();
            rightTextBox.Text = settingData.RightCutEdge.ToString();

            /// Image edit
            rotationComboBox.SelectedIndex = settingData.Rotation;
            flipComboBox.SelectedIndex = settingData.Flip;
            invertCheckBox.IsChecked = settingData.Invert;
            gainTextBox.Text = settingData.Gain.ToString();
            targetGainTextBox.Text = settingData.TargetGain.ToString();
            gainMarginTextBox.Text = settingData.GainMargin.ToString();
            refSatValueTextBox.Text = settingData.Refsat_Value.ToString();
            offsetMarginTextBox.Text = settingData.OffsetMargin.ToString();
            stdMarginTextBox.Text = settingData.StdMargin.ToString();
            brightMarginTextBox.Text = settingData.BrightMargin.ToString();
            surrMarginTextBox.Text = settingData.SurrMargin.ToString();

            /// DxD Parameter
            STimeTextBox.Text = settingData.S_Time;
            RTimeTextBox.Text = settingData.R_Time;
            ImageModeTextBox.Text = settingData.ImageMode;
            RefIntTextBox.Text = settingData.REF_INT;
            RefTftTextBox.Text = settingData.REF_TFT;
            LineTimeTextBox.Text = settingData.Line_Time;
            GateOnTimeTextBox.Text = settingData.Gate_On_Time;
            IntTimeTextBox.Text = settingData.Int_Time;
            ResetTimeTextBox.Text = settingData.Reset_Time;
            InitCountTextBox.Text = settingData.INIT_Count;
            IdleCountTextBox.Text = settingData.IDLE_Count;
            BeCountTextBox.Text = settingData.BE_Count;
            AECountTextBox.Text = settingData.AE_Count;
            SleepTimeTextBox.Text = settingData.Sleep_Time;
        }
        private void SaveSettingData()
        {
            /// network 
            settingData.IPs = IP_1.Text + "." + IP_2.Text + "." + IP_3.Text + "." + IP_4.Text;
            settingData.Port = int.Parse(port.Text);
            settingData.Timeout = int.Parse(timeout.Text);
            settingData.FrameWidth = int.Parse(framewidth.Text);
            settingData.FrameHeight = int.Parse(frameHeight.Text);
            settingData.DetectorModeAuto = (bool)detectorModeAuto.IsChecked;
            settingData.Sensitivity = int.Parse(sensitivity.Text);
            settingData.TriggerSensitivity = int.Parse(triggerSensitivity.Text);

            /// Location
            settingData.CalibrationFilePath = calibrationFileTextBox.Text;
            settingData.LogFilePath = logFileTextBox.Text;
            settingData.ImageFilePath = imageFileTextBox.Text;

            /// image Cut edge
            settingData.TopCutEdge = int.Parse(topTextBox.Text);
            settingData.BottomCutEdge = int.Parse(bottomTextBox.Text);
            settingData.LeftCutEdge = int.Parse(leftTextBox.Text);
            settingData.RightCutEdge = int.Parse(rightTextBox.Text);

            /// Image edit
            settingData.Rotation = rotationComboBox.SelectedIndex;
            settingData.Flip = flipComboBox.SelectedIndex;
            settingData.Invert = (bool)invertCheckBox.IsChecked;
            settingData.Gain = int.Parse(gainTextBox.Text);
            settingData.TargetGain = int.Parse(targetGainTextBox.Text);
            settingData.GainMargin = float.Parse(gainMarginTextBox.Text);
            settingData.Refsat_Value = int.Parse(refSatValueTextBox.Text);
            settingData.OffsetMargin = int.Parse(offsetMarginTextBox.Text);
            settingData.StdMargin = int.Parse(stdMarginTextBox.Text);
            settingData.BrightMargin = int.Parse(brightMarginTextBox.Text);
            settingData.SurrMargin = int.Parse(surrMarginTextBox.Text);

            /// DxD Parameter
            settingData.S_Time = STimeTextBox.Text;
            settingData.R_Time = RTimeTextBox.Text;
            settingData.ImageMode = ImageModeTextBox.Text;
            settingData.REF_INT = RefIntTextBox.Text;
            settingData.REF_TFT = RefTftTextBox.Text;
            settingData.Line_Time = LineTimeTextBox.Text;
            settingData.Gate_On_Time = GateOnTimeTextBox.Text;
            settingData.Int_Time = IntTimeTextBox.Text;
            settingData.Reset_Time = ResetTimeTextBox.Text;
            settingData.INIT_Count = InitCountTextBox.Text;
            settingData.IDLE_Count = IdleCountTextBox.Text;
            settingData.BE_Count = BeCountTextBox.Text;
            settingData.AE_Count = AECountTextBox.Text;
            settingData.Sleep_Time = SleepTimeTextBox.Text;
        }
        private static byte[] Convert16BitGrayScaleToRGB(ushort[] inBuffer, int width, int height)
        {
            byte[] outBuffer = new byte[width * height * 3];

            ushort lowValue = 0;
            
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    int inIndex = (y * width) + (x);
                    int outIndex = ((y * width) + (x)) * 3;

                    float value = inBuffer[inIndex];

                    float newValue;

                    if (value == lowValue) newValue = 0;
                    else newValue = 255;

                    //R, G, B
                    outBuffer[outIndex] = (byte)newValue;
                    outBuffer[outIndex + 1] = (byte)newValue;
                    outBuffer[outIndex + 2] = (byte)newValue;
                }
            }
            return outBuffer;
        }

        private static byte[] Convert16BitGrayScaleToRGB(ushort[] inBuffer, int width, int height, int level, int window)
        {
            byte[] outBuffer = new byte[width * height * 3];
            
            float lowValue = level - (window / 2.0f);
            float highValue = level + (window / 2.0f);

            // Step through the image by row
            for (int y = 0; y < height; y++)
            {
                // Step through the image by column
                for (int x = 0; x < width; x++)
                {
                    // Get inbuffer index and outbuffer index
                    int inIndex = (y * width) + (x);
                    int outIndex = ((y * width) + (x)) * 3;

                    float value = inBuffer[inIndex];
                    
                    float newValue;

                    if (value <= lowValue) newValue = 0;
                    else if (value >= highValue) newValue = 255;
                    else newValue = (value - lowValue) / (highValue - lowValue) * 255.0f;

                    //R, G, B
                    outBuffer[outIndex] = (byte)newValue;
                    outBuffer[outIndex+1] = (byte)newValue;
                    outBuffer[outIndex+2] = (byte)newValue;
                }
            }
            return outBuffer;
        }

        private static Bitmap CreateBitmapFromBytes(byte[] pixelValues, int width, int height)
        {
            //Create an image that will hold the image data
            Bitmap bmp = new Bitmap(width, height, PixelFormat.Format24bppRgb);

            //Get a reference to the images pixel data
            System.Drawing.Rectangle dimension = new System.Drawing.Rectangle(0, 0, bmp.Width, bmp.Height);
            BitmapData picData = bmp.LockBits(dimension, ImageLockMode.ReadWrite, bmp.PixelFormat);
            IntPtr pixelStartAddress = picData.Scan0;
            
            //Copy the pixel data into the bitmap structure
            System.Runtime.InteropServices.Marshal.Copy(pixelValues, 0, pixelStartAddress, pixelValues.Length);

            bmp.UnlockBits(picData);
            
            return bmp;
        }


        BitmapImage BitmapToImageSource(Bitmap bitmap)
        {
            using (MemoryStream memory = new MemoryStream())
            {
                bitmap.Save(memory, System.Drawing.Imaging.ImageFormat.Bmp);
                memory.Position = 0;
                BitmapImage bitmapimage = new BitmapImage();
                bitmapimage.BeginInit();
                bitmapimage.StreamSource = memory;
                bitmapimage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapimage.EndInit();

                return bitmapimage;
            }
        }

        private void calibrationButton_Click(object sender, RoutedEventArgs e)
        {
            int width = settingData.FrameWidth;
            int height = settingData.FrameHeight;
            int size = width * height;

            int darkCount = DarkImageListBox.Items.Count;
            
            ushort[] darkImages = new ushort[darkCount*size];
            for (int c = 0; c < darkCount; c++)
            {
                string fileName = (string)DarkImageListBox.Items.GetItemAt(c);
                string fullFileName = settingData.ImageFilePath + "\\" + fileName;
                FileStream darkImage = new FileStream(fullFileName, FileMode.Open, FileAccess.Read);
                for (int i = 0; i < size; i++)
                {
                    byte[] readPixel = new byte[2];
                    darkImage.Read(readPixel, 0, 2);

                    darkImages[c * size + i] = BitConverter.ToUInt16(readPixel, 0);
                }
                darkImage.Close();
            }

            int brightCount = BrightImageListBox.Items.Count;
            ushort[] brightImages = new ushort[brightCount*size];
            for (int c = 0; c < brightCount; c++)
            {
                string fileName = (string)BrightImageListBox.Items.GetItemAt(c);
                string fullFileName = settingData.ImageFilePath + "\\" + fileName;
                
                FileStream brightImage = new FileStream(fullFileName, FileMode.Open, FileAccess.Read);
                for (int i = 0; i < size; i++)
                {
                    byte[] readPixel = new byte[2];
                    brightImage.Read(readPixel, 0, 2);

                    brightImages[c*size + i] = BitConverter.ToUInt16(readPixel, 0);
                }
                brightImage.Close();
            }

            int ret = dataController.CreateCalibration(width, height, darkImages, darkCount, brightImages, brightCount);
            if( ret == 0)
            {
                MeanDarkImageListBox.Items.Add("Avgdark.raw");
                GainMapListBox.Items.Add("Gain.raw");
            }
        }

        private void calibrationDelButton_Click(object sender, RoutedEventArgs e)
        {
            int index = -1;

            index = DarkImageListBox.SelectedIndex;
            if (index != -1)
            {
                File.Delete(settingData.ImageFilePath + "\\" + DarkImageListBox.SelectedItem);
                DarkImageListBox.Items.RemoveAt(index);
                return;
            }
            index = BrightImageListBox.SelectedIndex;
            if (index != -1)
            {
                File.Delete(settingData.ImageFilePath + "\\" + BrightImageListBox.SelectedItem);
                BrightImageListBox.Items.RemoveAt(index);
                return;
            }
            index = MeanDarkImageListBox.SelectedIndex;
            if (index != -1)
            {
                File.Delete(settingData.CalibrationFilePath + "\\" + MeanDarkImageListBox.SelectedItem);
                MeanDarkImageListBox.Items.RemoveAt(index);
                return;
            }
            index = GainMapListBox.SelectedIndex;
            if (index != -1)
            {
                File.Delete(settingData.CalibrationFilePath + "\\" + GainMapListBox.SelectedItem);
                GainMapListBox.Items.RemoveAt(index);
                return;
            }
        }


        private void ListBoxGotFocus(object sender, RoutedEventArgs e)
        {
            DarkImageListBox.SelectedIndex = -1;
            BrightImageListBox.SelectedIndex = -1;
            MeanDarkImageListBox.SelectedIndex = -1;
            GainMapListBox.SelectedIndex = -1;
        }

        private void EditZoomClick(object sender, RoutedEventArgs e)
        {
            // 0.264f, 0.5f, 0.75f, 1.0f, 1.25f
            Button button = (Button)sender;
            if(button.Content.Equals("+"))
            {
                if (zoomFactor <= 0.264f) zoomFactor = 0.5f;
                else if (zoomFactor <= 0.5f) zoomFactor = 0.75f;
                else if (zoomFactor <= 0.75f) zoomFactor = 1.0f;
                else if (zoomFactor <= 1.0f) zoomFactor = 1.25f;
            }
            else // -
            {
                if (zoomFactor >= 1.25f) zoomFactor = 1.0f;
                else if (zoomFactor >= 1.0f) zoomFactor = 0.75f;
                else if (zoomFactor >= 0.75f) zoomFactor = 0.5f;
                else if (zoomFactor >= 0.5f) zoomFactor = 0.264f;
            }
            calibrationImageScaleTransform.ScaleX = zoomFactor;
            calibrationImageScaleTransform.ScaleY = zoomFactor;
            acquistionImageScaleTransform.ScaleX = zoomFactor;
            acquistionImageScaleTransform.ScaleY = zoomFactor;
            defectCalibrationImageScaleTransform.ScaleX = zoomFactor;
            defectCalibrationImageScaleTransform.ScaleY = zoomFactor;

            double Xoffset = double.Parse(xValueTextBox.Text) / 2 * (calibrationImage.Width * zoomFactor) / settingData.FrameWidth;
            double Yoffset = double.Parse(yValueTextBox.Text) / 2 * (calibrationImage.Height * zoomFactor) / settingData.FrameHeight;

            calibrationScrollViewer.ScrollToHorizontalOffset(Xoffset - (calibrationImage.Width)/ 2.0);
            calibrationScrollViewer.ScrollToVerticalOffset(Yoffset - (calibrationImage.Height) / 2.0);
            acquistionScrollViewer.ScrollToHorizontalOffset(Xoffset - (calibrationImage.Width) / 2.0);
            acquistionScrollViewer.ScrollToVerticalOffset(Yoffset - (calibrationImage.Height) / 2.0);
            defectCalibrationScrollViewer.ScrollToHorizontalOffset(Xoffset - (calibrationImage.Width) / 2.0);
            defectCalibrationScrollViewer.ScrollToVerticalOffset(Yoffset - (calibrationImage.Height) / 2.0);
            
        }

        private void calibrationEditResetButtion_Click(object sender, RoutedEventArgs e)
        {
            EditReset();
        }

        private void EditReset()
        {
            zoomFactor = 1.0f;
            windowValue = 16383;
            levelValue = 8192;
            windowValueTextBox.Text = "" + windowValue;
            levelValueTextBox.Text = "" + levelValue;
            xValueTextBox.Text = "" + (settingData.FrameWidth);
            yValueTextBox.Text = "" + (settingData.FrameHeight);

            windowValueTextBox2.Text = "" + windowValue;
            levelValueTextBox2.Text = "" + levelValue;
            xValueTextBox2.Text = "" + (settingData.FrameWidth);
            yValueTextBox2.Text = "" + (settingData.FrameHeight);

            windowValueTextBox3.Text = "" + windowValue;
            levelValueTextBox3.Text = "" + levelValue;
            xValueTextBox3.Text = "" + (settingData.FrameWidth);
            yValueTextBox3.Text = "" + (settingData.FrameHeight);
            
            pixelValueTextBox.Text = "";
            
            ImageRefresh(0, settingData.FrameWidth, settingData.FrameHeight);
        }

        private void calbrationImage_MouseMove(object sender, MouseEventArgs e)
        {
            System.Windows.Controls.Image image = (System.Windows.Controls.Image)sender;
            downPosition = e.GetPosition(image);
            Point point = e.GetPosition(image);
            Point distance = new Point();
            distance.X = downPosition.X - point.X;
            distance.Y = downPosition.Y - point.Y;
            int frameWidth = settingData.FrameWidth;
            int frameHeight = settingData.FrameHeight;
            double xFactor = frameWidth / image.ActualWidth;
            double yFactor = frameHeight / image.ActualHeight;
            string statusText = "";
            
            if (moveMode)
            {
                calibrationScrollViewer.ScrollToHorizontalOffset(calibrationScrollViewer.HorizontalOffset + distance.X * zoomFactor);
                calibrationScrollViewer.ScrollToVerticalOffset(calibrationScrollViewer.VerticalOffset + distance.Y * zoomFactor);
                statusText = "move";
            }
            else if(editMode)
            {
                levelValue += (int)(-distance.X) * 3;
                windowValue += (int)(-distance.Y) * 3;
                ImageRefresh(0, frameWidth, frameHeight);
                statusText = "edit level = " + levelValue + " window = " + windowValue;
                levelValueTextBox.Text = levelValue.ToString();
                windowValueTextBox.Text = windowValue.ToString();
            }
            else
            { 
                mouseX = (int)(point.X * xFactor);
                mouseY = (int)(point.Y * yFactor);
                int position = mouseX + mouseY * frameWidth;
            
                if(position < showingImage.Length)
                    pixelValue = showingImage[position];
                
                statusText = string.Format("zoom {0:0.00}% level {1} window {2} x={3} y={4} value={5}",
                    (xFactor / zoomFactor * 100.0), levelValue, windowValue, mouseX, mouseY, pixelValue);
            }
            status.Text = statusText;
            downPosition = point;
        }
       
        private void calbrationImage_MouseDown(object sender, MouseButtonEventArgs e)
        {
            System.Windows.Controls.Image image = (System.Windows.Controls.Image)sender;
            downPosition = e.GetPosition(image);

            if (e.RightButton == MouseButtonState.Pressed && Keyboard.Modifiers == ModifierKeys.Control)
            {
                moveMode = true;
            }
            else if(e.RightButton == MouseButtonState.Pressed)
            {
                editMode = true;
            }
            else if(e.LeftButton == MouseButtonState.Pressed && menuTabContrl.SelectedIndex == 3)
            {

                Point point = e.GetPosition(image);

                int frameWidth = settingData.FrameWidth;
                int frameHeight = settingData.FrameHeight;
                double xFactor = frameWidth / image.ActualWidth;
                double yFactor = frameHeight / image.ActualHeight;

                mouseX = (int)(point.X * xFactor);
                mouseY = (int)(point.Y * yFactor);
                ushort val;

                if (Radio_One.IsChecked == true)
                    val = 1;
                else
                    val = 0;

                if (Radio_V.IsChecked == true)
                {
                    BPMHistoryAdd(mouseX, mouseY, 1, val);
                    for (int h = 0; h < frameHeight; h++)
                    {
                        showingImage[h * frameWidth + mouseX] = val;
                        if (val == 1)
                        {
                            rgbDatas[(h * frameWidth + mouseX) * 3 + 0] = 0;
                            rgbDatas[(h * frameWidth + mouseX) * 3 + 1] = 0;
                            rgbDatas[(h * frameWidth + mouseX) * 3 + 2] = 255;
                        }
                        else
                        {
                            rgbDatas[(h * frameWidth + mouseX) * 3 + 0] = 0;
                            rgbDatas[(h * frameWidth + mouseX) * 3 + 1] = 255;
                            rgbDatas[(h * frameWidth + mouseX) * 3 + 2] = 0;
                        }
                    }
                }
                else if (Radio_H.IsChecked == true)
                {
                    BPMHistoryAdd(mouseX, mouseY, 2, val);

                    for (int w = 0; w < frameWidth; w++)
                    {
                        showingImage[mouseY * frameWidth + w] = val;
                        if (val == 1)
                        {
                            rgbDatas[(mouseY * frameWidth + w) * 3 + 0] = 0;
                            rgbDatas[(mouseY * frameWidth + w) * 3 + 1] = 0;
                            rgbDatas[(mouseY * frameWidth + w) * 3 + 2] = 255;
                        }
                        else
                        {
                            rgbDatas[(mouseY * frameWidth + w) * 3 + 0] = 0;
                            rgbDatas[(mouseY * frameWidth + w) * 3 + 1] = 255;
                            rgbDatas[(mouseY * frameWidth + w) * 3 + 2] = 0;
                        }
                    }
                }
                else
                {
                    BPMHistoryAdd(mouseX, mouseY, 0, val);
                    showingImage[mouseY * frameWidth + mouseX] = val;
                    if (val == 1)
                    {
                        rgbDatas[(mouseY * frameWidth + mouseX) * 3 + 0] = 0;
                        rgbDatas[(mouseY * frameWidth + mouseX) * 3 + 1] = 0;
                        rgbDatas[(mouseY * frameWidth + mouseX) * 3 + 2] = 255;
                    }
                    else
                    {
                        rgbDatas[(mouseY * frameWidth + mouseX) * 3 + 0] = 0;
                        rgbDatas[(mouseY * frameWidth + mouseX) * 3 + 1] = 255;
                        rgbDatas[(mouseY * frameWidth + mouseX) * 3 + 2] = 0;
                    }
                }
                ImageRefresh(5, frameWidth, frameHeight);
            }

        }

        private void calbrationImage_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Released)
            {
            }
            if (e.RightButton == MouseButtonState.Released)
            {
                moveMode = false;
                editMode = false;
            }
        }

        private void calbrationImage_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            System.Windows.Controls.Image image = (System.Windows.Controls.Image)sender;
            Point point = e.GetPosition(image);
            e.Handled = true;
            if (e.Delta > 0)
            {
                zoomFactor += 0.1f;
            }
            else if(e.Delta < 0)
            {
                zoomFactor -= 0.1f;
            }
            // 0.264f, 0.5f, 0.75f, 1.0f, 1.25f
            if (zoomFactor < 0.264f)
            {
                zoomFactor = 0.264f;
            }
            //else if(zoomFactor > 1.25f)
            //{
            //    zoomFactor = 1.25f;
            //}

            calibrationImageScaleTransform.ScaleX = zoomFactor;
            calibrationImageScaleTransform.ScaleY = zoomFactor;
            acquistionImageScaleTransform.ScaleX = zoomFactor;
            acquistionImageScaleTransform.ScaleY = zoomFactor;
            defectCalibrationImageScaleTransform.ScaleX = zoomFactor;
            defectCalibrationImageScaleTransform.ScaleY = zoomFactor;


            int frameWidth = settingData.FrameWidth;
            int frameHeight = settingData.FrameHeight;
            double xFactor = frameWidth / image.ActualWidth;
            double yFactor = frameHeight / image.ActualHeight;
            mouseX = (int)(point.X * xFactor);
            mouseY = (int)(point.Y * yFactor);

            double Xoffset = mouseX * (image.Width * zoomFactor) / settingData.FrameWidth;
            double Yoffset = mouseY * (image.Height * zoomFactor) / settingData.FrameHeight;

            calibrationScrollViewer.ScrollToHorizontalOffset(Xoffset - point.X);
            calibrationScrollViewer.ScrollToVerticalOffset(Yoffset - point.Y);
            acquistionScrollViewer.ScrollToHorizontalOffset(Xoffset - point.X);
            acquistionScrollViewer.ScrollToVerticalOffset(Yoffset - point.Y);
            defectCalibrationScrollViewer.ScrollToHorizontalOffset(Xoffset - point.X);
            defectCalibrationScrollViewer.ScrollToVerticalOffset(Yoffset - point.Y);
        }

        private void image_Drop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);

                ImageOpen(-1, files[0], settingData.FrameWidth, settingData.FrameHeight);
            }
        }

        private void defectCalibrationImage_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            

        }

        private void BPMHistoryAdd(int mouseX, int mouseY, int type, ushort val)
        {
            string history = "(";
            switch (type) { 
                case 0:
                    history = history + mouseX + ", " + mouseY + ")";
                    break;
                case 1:
                    history = history + ":, " + mouseY + ")";
                    break;
                case 2:
                    history = history + mouseX + ", :"  + ")";
                    break;
            }
            history += "<-" + val;
            if (BadPixelHistory.Items.Count >= 10) BadPixelHistory.Items.RemoveAt(0);
            BadPixelHistory.Items.Add(history);
        }

        private void DefectCalibrationResetButton_Click(object sender, RoutedEventArgs e)
        {
            ImageOpen(5, "BPM.raw", settingData.FrameWidth, settingData.FrameHeight);
        }

        private void defectCalibrationSaveButton_Click(object sender, RoutedEventArgs e)
        {
            string fullFileName = settingData.CalibrationFilePath + "\\BPM.raw";

            if (File.Exists(fullFileName)) File.Delete(fullFileName);
            FileStream fileStream = new FileStream(fullFileName, FileMode.Create | FileMode.Append, FileAccess.Write);
            for (int i = 0; i < showingImage.Length; i++)
            {
                fileStream.Write(BitConverter.GetBytes(showingImage[i]), 0, 2);
            }

            fileStream.Close();
        }

        private void ImageOpenButton_Click(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            String name = button.Content.ToString();
            if(name.Equals("Dark Average"))
            {
                ImageOpen(2, "Avgdark.raw", settingData.FrameWidth, settingData.FrameHeight);
            }
            else if(name.Equals("Offset Map"))
            {
                ImageOpen(3, "Offset.raw", settingData.FrameWidth, settingData.FrameHeight);
            }
            else if(name.Equals("Gain Map"))
            {
                ImageOpen(4, "Gain.raw", settingData.FrameWidth, settingData.FrameHeight);
            }
            else if(name.Equals("Bad Pixel Map"))
            {
                ImageOpen(5, "BPM.raw", settingData.FrameWidth, settingData.FrameHeight);
            }
        }

        private void ImageListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var listbox = (ListBox)sender;
            if(listbox.SelectedIndex == -1) return;

            if (listbox.Name.Equals("DarkImageListBox"))
            {
                ImageOpen(0, listbox.SelectedItem.ToString(), settingData.FrameWidth, settingData.FrameHeight);
            }
            else if (listbox.Name.Equals("BrightImageListBox"))
            {
                ImageOpen(1, listbox.SelectedItem.ToString(), settingData.FrameWidth, settingData.FrameHeight);
            }
            else if (listbox.Name.Equals("MeanDarkImageListBox"))
            {
                ImageOpen(2, listbox.SelectedItem.ToString(), settingData.FrameWidth, settingData.FrameHeight);
            }
            else if (listbox.Name.Equals("GainMapListBox"))
            {
                ImageOpen(4, listbox.SelectedItem.ToString(), settingData.FrameWidth, settingData.FrameHeight);
            }
            else if(listbox.Name.Equals("AcqusitionImageListBox"))
            {
                int width = settingData.FrameWidth - settingData.LeftCutEdge - settingData.RightCutEdge;
                int height = settingData.FrameHeight - settingData.TopCutEdge - settingData.BottomCutEdge;

                ImageOpen(6, listbox.SelectedItem.ToString(), width, height);
            }
        }

        private void menuTabContrl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            {
                int tabItem = ((sender as TabControl)).SelectedIndex;
                if (e.Source is TabControl) // This is a soultion of those problem.
                {
                    switch (tabItem)
                    {
                        case 1:     // Calibration
                            break;
                        case 2:     // Acquisition
                            break;
                        case 3:     // BPM Check
                            ImageOpen(5, "BPM.raw", settingData.FrameWidth, settingData.FrameHeight);
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            // dark
            int count = DarkImageListBox.Items.Count;
            for(int c = 0; c < count; c++)
            {
                ImageDelete(0, DarkImageListBox.Items.GetItemAt(c).ToString());
            }
            // bright
            count = BrightImageListBox.Items.Count;
            for (int c = 0; c < count; c++)
            {
                ImageDelete(0, BrightImageListBox.Items.GetItemAt(c).ToString());
            }
            // acq
            count = AcqusitionImageListBox.Items.Count;
            for (int c = 0; c < count; c++)
            {
                ImageDelete(0, AcqusitionImageListBox.Items.GetItemAt(c).ToString());
            }
            // calibrated
            {
                ImageDelete(1, "Avgdark.raw");
                ImageDelete(1, "Offset.raw");
                ImageDelete(1, "Gain.raw");
                ImageDelete(1, "BPM.raw");
            }
        }
    }
}
