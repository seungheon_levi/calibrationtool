﻿using Microsoft.WindowsAPICodePack.Shell.Interop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace CalibrationTool
{
    class SettingData
    {
        public string IPs { get; set; }
        public int Port { get; set; }
        public int Timeout { get; set; }
        public bool DetectorModeAuto { get; set; } // true Auto false HandShaking
        public int Sensitivity { get; set; }
        public int TriggerSensitivity { get; set; }
        public int FrameWidth { get; set; }
        public int FrameHeight { get; set; }
        public int WindowTime { get; set; } // ??
        // H/W setting infomation
        public string S_Time { get; set; }
        public string R_Time { get; set; }
        public string ImageMode { get; set; }
        public string REF_INT { get; set; }
        public string REF_TFT { get; set; }
        public string Line_Time { get; set; }
        public string Gate_On_Time { get; set; }
        public string Int_Time { get; set; }
        public string Reset_Time { get; set; }
        public string INIT_Count { get; set; }
        public string IDLE_Count { get; set; }
        public string BE_Count { get; set; }
        public string AE_Count { get; set; }
        public string Sleep_Time { get; set; }


        public string CalibrationFilePath { get; set; }
        public string LogFilePath { get; set; }
        public string ImageFilePath { get; set; }

        public int TopCutEdge { get; set; }
        public int BottomCutEdge { get; set; }
        public int LeftCutEdge { get; set; }
        public int RightCutEdge { get; set; }

        public int Rotation { get; set; }
        public int Flip { get; set; }
        public bool Invert { get; set; }
        public int Gain { get; set; }
        public int TargetGain { get; set; }
        public float GainMargin { get; set; }
        public int Refsat_Value { get; set; }
        public int OffsetMargin { get; set; }
        public int StdMargin { get; set; }
        public int BrightMargin { get; set; }
        public int SurrMargin { get; set; }

        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section, string key, string val, string filePath);
        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retVal, int size, string filePath);


        public SettingData()
        {

            if(MessageBox.Show("User Data Load", "Data Load", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                LoadSettingInfo(AppDomain.CurrentDomain.BaseDirectory + "data\\USR_Setting.cfg");
            }
            else
            {
                LoadSettingInfo(AppDomain.CurrentDomain.BaseDirectory + "data\\ORG_Setting.cfg");
            }
        }
        bool LoadSettingInfo(string filePath)
        {
            StringBuilder sb = new StringBuilder();
            sb.Length = 512;
            try
            {

                GetPrivateProfileString("Setting", "IP", "", sb, sb.Capacity, filePath); IPs = sb.ToString();
                GetPrivateProfileString("Setting", "Port", "", sb, sb.Capacity, filePath); Port = int.Parse(sb.ToString());
                GetPrivateProfileString("Setting", "Timeout", "", sb, sb.Capacity, filePath); Timeout = int.Parse(sb.ToString());
                GetPrivateProfileString("Setting", "FrameWidth", "", sb, sb.Capacity, filePath); FrameWidth = int.Parse(sb.ToString());
                GetPrivateProfileString("Setting", "FrameHeight", "", sb, sb.Capacity, filePath); FrameHeight = int.Parse(sb.ToString());
                GetPrivateProfileString("Setting", "Mode", "", sb, sb.Capacity, filePath); DetectorModeAuto = (0 == int.Parse(sb.ToString()));
                GetPrivateProfileString("Setting", "Sensitivity", "", sb, sb.Capacity, filePath); Sensitivity = int.Parse(sb.ToString());
                GetPrivateProfileString("Setting", "Trigger", "", sb, sb.Capacity, filePath); TriggerSensitivity = int.Parse(sb.ToString());
                GetPrivateProfileString("Setting", "Window", "", sb, sb.Capacity, filePath); WindowTime = int.Parse(sb.ToString());
                GetPrivateProfileString("Setting", "S_Time", "", sb, sb.Capacity, filePath); S_Time = sb.ToString();
                GetPrivateProfileString("Setting", "R_Time", "", sb, sb.Capacity, filePath); R_Time = sb.ToString();
                GetPrivateProfileString("Setting", "Imaging Mode", "", sb, sb.Capacity, filePath); ImageMode = sb.ToString();
                GetPrivateProfileString("Setting", "REF_INT", "", sb, sb.Capacity, filePath); REF_INT = sb.ToString();
                GetPrivateProfileString("Setting", "REF_TFT", "", sb, sb.Capacity, filePath); REF_TFT = sb.ToString();
                GetPrivateProfileString("Setting", "Line Time", "", sb, sb.Capacity, filePath); Line_Time = sb.ToString();
                GetPrivateProfileString("Setting", "Gate On Time", "", sb, sb.Capacity, filePath); Gate_On_Time = sb.ToString();
                GetPrivateProfileString("Setting", "Int_Time", "", sb, sb.Capacity, filePath); Int_Time = sb.ToString();
                GetPrivateProfileString("Setting", "Reset Time", "", sb, sb.Capacity, filePath); Reset_Time = sb.ToString();
                GetPrivateProfileString("Setting", "INIT Count", "", sb, sb.Capacity, filePath); INIT_Count = sb.ToString();
                GetPrivateProfileString("Setting", "IDLE Count", "", sb, sb.Capacity, filePath); IDLE_Count = sb.ToString();
                GetPrivateProfileString("Setting", "BE Count", "", sb, sb.Capacity, filePath); BE_Count = sb.ToString();
                GetPrivateProfileString("Setting", "AE Count", "", sb, sb.Capacity, filePath); AE_Count = sb.ToString();
                GetPrivateProfileString("Setting", "Sleep_Time", "", sb, sb.Capacity, filePath); Sleep_Time = sb.ToString();

                GetPrivateProfileString("Folder", "Calibration", "", sb, sb.Capacity, filePath); CalibrationFilePath = sb.ToString();
                GetPrivateProfileString("Folder", "Log", "", sb, sb.Capacity, filePath); LogFilePath = sb.ToString();
                GetPrivateProfileString("Folder", "Image", "", sb, sb.Capacity, filePath); ImageFilePath = sb.ToString();

                GetPrivateProfileString("CutEdge", "Left", "", sb, sb.Capacity, filePath); LeftCutEdge = int.Parse(sb.ToString());
                GetPrivateProfileString("CutEdge", "Right", "", sb, sb.Capacity, filePath); RightCutEdge = int.Parse(sb.ToString());
                GetPrivateProfileString("CutEdge", "Top", "", sb, sb.Capacity, filePath); TopCutEdge = int.Parse(sb.ToString());
                GetPrivateProfileString("CutEdge", "Bottom", "", sb, sb.Capacity, filePath); BottomCutEdge = int.Parse(sb.ToString());

                GetPrivateProfileString("Info", "Rotate", "", sb, sb.Capacity, filePath); Rotation = int.Parse(sb.ToString());
                GetPrivateProfileString("Info", "Flip", "", sb, sb.Capacity, filePath); Flip = int.Parse(sb.ToString());
                GetPrivateProfileString("Info", "Invert", "", sb, sb.Capacity, filePath); Invert = (0 == int.Parse(sb.ToString()));
                GetPrivateProfileString("Info", "Gain", "", sb, sb.Capacity, filePath); Gain = int.Parse(sb.ToString());
                GetPrivateProfileString("Info", "Gain_Margin", "", sb, sb.Capacity, filePath); GainMargin = float.Parse(sb.ToString());
                GetPrivateProfileString("Info", "Offset_Margin", "", sb, sb.Capacity, filePath); OffsetMargin = int.Parse(sb.ToString());
                GetPrivateProfileString("Info", "Target_Gain", "", sb, sb.Capacity, filePath); TargetGain = int.Parse(sb.ToString());
                GetPrivateProfileString("Info", "Ref_sat_value", "", sb, sb.Capacity, filePath); Refsat_Value = int.Parse(sb.ToString());
                GetPrivateProfileString("Info", "Std_Margin", "", sb, sb.Capacity, filePath); StdMargin = int.Parse(sb.ToString());
                GetPrivateProfileString("Info", "Bright_Margin", "", sb, sb.Capacity, filePath); BrightMargin = int.Parse(sb.ToString());
                GetPrivateProfileString("Info", "Surr_Margin", "", sb, sb.Capacity, filePath); SurrMargin = int.Parse(sb.ToString());

            }
            catch (Exception)
            {
                MessageBox.Show("LoadFailed");


                IPs = "2.2.22.158";
                Port = 5050;
                Timeout = 30;
                FrameWidth = 3072;
                FrameHeight = 3072;
                DetectorModeAuto = true;
                Sensitivity = 1;
                TriggerSensitivity = 256;
                WindowTime = 1;
                S_Time = "" + 10;
                R_Time = "" + 30;
                ImageMode = "" + 0x0001;
                REF_INT = "" + 0x0020;
                REF_TFT = "" + 0x0020;
                Line_Time = "" + 0x1356;
                Gate_On_Time = "" + 0x0252;
                Int_Time = "" + 0x0339;
                Reset_Time = "" + 0x0108;
                INIT_Count = "" + 0x000a;
                IDLE_Count = "" + 0x0001;
                BE_Count = "" + 0x0005;
                AE_Count = "" + 0x000a;
                Sleep_Time = "" + 0;

                CalibrationFilePath = ".\\data\\Calibration";
                LogFilePath = ".\\data\\Log";
                ImageFilePath = ".\\data\\Image";

                LeftCutEdge = 0;
                RightCutEdge = 0;
                TopCutEdge = 0;
                BottomCutEdge = 0;

                Rotation = 0;
                Flip = 0;
                Invert = false;
                Gain = 0;
                GainMargin = 0.3f;
                OffsetMargin = 45;
                TargetGain = 1;
                Refsat_Value = 13000;
                StdMargin = 20;
                BrightMargin = 100;
                SurrMargin = 25;

            }
            return true;
        }

        internal void SaveSettingFile()
        {
            SaveSettingInfo("./data/USR_Setting.cfg");
        }

        bool SaveSettingInfo(string filePath)
        {
            WritePrivateProfileString("Setting", "IP", IPs, filePath);
            WritePrivateProfileString("Setting", "Port", Port.ToString(), filePath);
            WritePrivateProfileString("Setting", "Timeout", Timeout.ToString(), filePath);
            WritePrivateProfileString("Setting", "FrameWidth", FrameWidth.ToString(), filePath);
            WritePrivateProfileString("Setting", "FrameHeight", FrameHeight.ToString(), filePath);
            WritePrivateProfileString("Setting", "Mode", DetectorModeAuto?"2":"0", filePath);
            WritePrivateProfileString("Setting", "Sensitivity", Sensitivity.ToString(), filePath);
            WritePrivateProfileString("Setting", "Trigger", TriggerSensitivity.ToString(), filePath);
            WritePrivateProfileString("Setting", "Window", WindowTime.ToString(), filePath);
            WritePrivateProfileString("Setting", "S_Time", S_Time, filePath);
            WritePrivateProfileString("Setting", "R_Time", R_Time, filePath);
            WritePrivateProfileString("Setting", "Imaging Mode", ImageMode, filePath);
            WritePrivateProfileString("Setting", "REF_INT", REF_INT, filePath);
            WritePrivateProfileString("Setting", "REF_TFT", REF_TFT, filePath);
            WritePrivateProfileString("Setting", "Line Time", Line_Time, filePath);
            WritePrivateProfileString("Setting", "Gate On Time", Gate_On_Time, filePath);
            WritePrivateProfileString("Setting", "Int_Time", Int_Time, filePath);
            WritePrivateProfileString("Setting", "Reset Time", Reset_Time, filePath);
            WritePrivateProfileString("Setting", "INIT Count", INIT_Count, filePath);
            WritePrivateProfileString("Setting", "IDLE Count", IDLE_Count, filePath);
            WritePrivateProfileString("Setting", "BE Count", BE_Count, filePath);
            WritePrivateProfileString("Setting", "AE Count", AE_Count, filePath);
            WritePrivateProfileString("Setting", "Sleep_Time", Sleep_Time, filePath);

            WritePrivateProfileString("Folder", "Calibration", CalibrationFilePath, filePath);
            WritePrivateProfileString("Folder", "Log", LogFilePath, filePath);
            WritePrivateProfileString("Folder", "Image", ImageFilePath, filePath);

            WritePrivateProfileString("CutEdge", "Left", LeftCutEdge.ToString(), filePath);
            WritePrivateProfileString("CutEdge", "Right", RightCutEdge.ToString(), filePath);
            WritePrivateProfileString("CutEdge", "Top", TopCutEdge.ToString(), filePath);
            WritePrivateProfileString("CutEdge", "Bottom", BottomCutEdge.ToString(), filePath);

            WritePrivateProfileString("Info", "Rotate", Rotation.ToString(), filePath);
            WritePrivateProfileString("Info", "Flip", Flip.ToString(), filePath);
            WritePrivateProfileString("Info", "Invert", Invert?"1":"0", filePath);
            WritePrivateProfileString("Info", "Gain", Gain.ToString(), filePath);
            WritePrivateProfileString("Info", "Gain_Margin", GainMargin.ToString(), filePath);
            WritePrivateProfileString("Info", "Offset_Margin", OffsetMargin.ToString(), filePath);
            WritePrivateProfileString("Info", "Target_Gain", TargetGain.ToString(), filePath);
            WritePrivateProfileString("Info", "Ref_sat_value", Refsat_Value.ToString(), filePath);
            WritePrivateProfileString("Info", "Std_Margin", StdMargin.ToString(), filePath);
            WritePrivateProfileString("Info", "Bright_Margin", BrightMargin.ToString(), filePath);
            WritePrivateProfileString("Info", "Surr_Margin", SurrMargin.ToString(), filePath);

            return true;
        }


    }
}
