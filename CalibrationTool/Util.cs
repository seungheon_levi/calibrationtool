﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace CalibrationTool
{
    class Util
    {
        private ListBox logListBox;
        private SettingData settingData;
        private FileStream fileStream;
        private string fileName;
        public Util(ListBox logListBox, SettingData settingData)
        {
            this.logListBox = logListBox;
            this.settingData = settingData;
            DateTime dateTime = DateTime.Now;
            fileName = settingData.LogFilePath + "\\" + "Log_" + dateTime.ToString("yyyyMMddHHmmss") + ".txt";
            if (fileStream != null) fileStream.Close();
            fileStream = new FileStream(fileName, FileMode.Create | FileMode.Append, FileAccess.Write);
           
        }

        public void LogFormat(string log)
        {
            DateTime dateTime = DateTime.Now;
            string contents = "[" + dateTime.ToString("yyyy.MM.dd HH:mm:ss") + "]    " + log + "\n";
            WriteLog(contents);
        }

        private void WriteLog(string logString)
        {
            logListBox.Items.Add(logString);
            logListBox.SelectedIndex = logListBox.Items.Count - 1;
            logListBox.ScrollIntoView(logListBox.SelectedItem);

            if (fileStream != null)
                fileStream.Write(Encoding.UTF8.GetBytes(logString), 0, logString.Length);
            else
            {
                fileStream = new FileStream(fileName, FileMode.Create | FileMode.Append, FileAccess.Write);
                fileStream.Write(Encoding.UTF8.GetBytes(logString), 0, logString.Length);
            }
        }
    }
}
