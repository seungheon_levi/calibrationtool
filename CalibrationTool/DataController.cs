﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace CalibrationTool
{
    class DataController
    {
        private SettingData settingData;
        private Util util;

        ushort[] dark;
        short[] offset;
        float[] gain;
        ushort[] bpm;


        [DllImport("kalDRCalibration.dll", CallingConvention = CallingConvention.Cdecl, EntryPoint = "CreateCalibrationInfo0")]
        public unsafe static extern int CreateCalibrationInfo(int width, int height,
            ushort[] darks, int darkCount,
            ushort[] brights, int brightCount, 
            double stdMargin, double target_gain, double gain_margin, double offset_margin, double sur_margin,
            ushort[] meanDark, short[] offsetMap, float[] gainMap, ushort[] BPM);

        [DllImport("kalDRCalibration.dll", CallingConvention = CallingConvention.Cdecl, EntryPoint = "Calibrate0")]
        public static extern int Calibrate(ushort[] image, int width, int height,
            ushort[] dark, short[] offset, float[] gain, ushort[] bpm, int refSaturation, ushort[] result);


        public DataController(SettingData settingData, Util util, int width, int height)
        {
            this.settingData = settingData;
            this.util = util;

            int size = width * height;

            dark = new ushort[size];
            offset = new short[size];
            gain = new float[size];
            bpm = new ushort[size];
        }

        [Serializable]
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct DETECTOR_PKT
        {
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
            public byte[] DID;
            public ushort OPCode;
            public ushort Size;

            public DETECTOR_PKT(byte[] _DID, ushort _OPCode, ushort _Size)
            {
                DID = new byte[4];
                DID = _DID;
                OPCode = _OPCode;
                Size = _Size;
            }
            public DETECTOR_PKT(byte[] data, int size)
            {
                if (size == 0)
                {
                    DID = new byte[4];
                    OPCode = 0;
                    Size = 0;
                }
                else
                {
                    DID = new byte[4];
                    DID[0] = data[0];
                    DID[1] = data[1];
                    DID[2] = data[2];
                    DID[3] = data[3];
                    OPCode = BitConverter.ToUInt16(data, 4);
                    Size = BitConverter.ToUInt16(data, 6);
                }
            }
        }
        public struct DETECTOR_PKT_IMAGE_PACKET
        {
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
            public byte[] DID;
            public ushort OPCode;
            public ushort Size;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
            public ushort[] Param;

            public DETECTOR_PKT_IMAGE_PACKET(byte[] data, int size)
            {
                if (size == 0)
                {
                    DID = new byte[4];
                    OPCode = 0;
                    Size = 0;
                    Param = new ushort[2];
                }
                else
                {
                    DID = new byte[4];
                    DID[0] = data[0];
                    DID[1] = data[1];
                    DID[2] = data[2];
                    DID[3] = data[3];
                    OPCode = BitConverter.ToUInt16(data, 4);
                    Size = BitConverter.ToUInt16(data, 6);
                    Param = new ushort[2];
                    Param[0] = BitConverter.ToUInt16(data, 8);
                    Param[1] = BitConverter.ToUInt16(data, 10);
                }
            }
        }
        struct DETECTOR_PKT_INITIALIZE
        {
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
            public byte[] DID;
            public ushort OPCode;
            public ushort Size;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 32)]
            public ushort[] Param;
            public DETECTOR_PKT_INITIALIZE(byte[] _DID, ushort _OPCode, ushort _Size, ushort[] _Param)
            {
                DID = new byte[4];
                DID = _DID;
                OPCode = _OPCode;
                Size = _Size;
                Param = new ushort[32];
                Param = _Param;
            }
        };

        internal void ApplySettingData()
        {
            byte[] did = new byte[4];
            did[0] = (byte)'A';
            did[1] = (byte)'U';
            did[2] = (byte)'R';
            did[3] = (byte)'A';
            ushort opcode = 0xaa21;
            ushort size = 0x0020;

            ushort[] param = new ushort[32];
            param[0] = (ushort)settingData.FrameHeight;
            param[1] = (ushort)settingData.FrameWidth;
            param[2] = (ushort)settingData.Sensitivity;
            param[3] = 0x0000;
            param[4] = (ushort)(settingData.DetectorModeAuto == true ? 0x0000 : 0x0002);
            param[5] = (ushort)settingData.TriggerSensitivity;
            param[6] = (ushort)settingData.WindowTime;
            param[7] = 0x0000;
            param[8] = 0x0000;
            param[9] = 0x0009;

            param[10] = ushort.Parse(settingData.ImageMode.Substring(2, 4), System.Globalization.NumberStyles.HexNumber);
            param[11] = ushort.Parse(settingData.REF_INT.Substring(2, 4), System.Globalization.NumberStyles.HexNumber);
            param[12] = ushort.Parse(settingData.REF_TFT.Substring(2, 4), System.Globalization.NumberStyles.HexNumber);
            param[13] = ushort.Parse(settingData.Line_Time.Substring(2, 4), System.Globalization.NumberStyles.HexNumber);
            param[14] = ushort.Parse(settingData.Gate_On_Time.Substring(2, 4), System.Globalization.NumberStyles.HexNumber);
            param[15] = ushort.Parse(settingData.Int_Time.Substring(2, 4), System.Globalization.NumberStyles.HexNumber);
            param[16] = ushort.Parse(settingData.Reset_Time.Substring(2, 4), System.Globalization.NumberStyles.HexNumber);
            param[17] = ushort.Parse(settingData.INIT_Count.Substring(2, 4), System.Globalization.NumberStyles.HexNumber);
            param[18] = ushort.Parse(settingData.IDLE_Count.Substring(2, 4), System.Globalization.NumberStyles.HexNumber);
            param[19] = ushort.Parse(settingData.BE_Count.Substring(2, 4), System.Globalization.NumberStyles.HexNumber);
            param[20] = ushort.Parse(settingData.AE_Count.Substring(2, 4), System.Globalization.NumberStyles.HexNumber);
            param[21] = 0xffff;
            param[22] = 0xffff;
            param[23] = 0xffff;
            param[24] = 0xffff;
            param[25] = 0xffff;
            param[26] = 0xffff;
            param[27] = 0xffff;
            param[28] = 0xffff;
            param[29] = 0xffff;
            param[30] = 0xffff;
            param[31] = 0xffff;

            DETECTOR_PKT_INITIALIZE pkt = new DETECTOR_PKT_INITIALIZE(did, opcode, size, param);

            {
                Socket socket = new Socket(SocketType.Stream, ProtocolType.Tcp);

                try
                {
                    socket.ReceiveTimeout = 5000; // timeout 5sec
                    IPEndPoint detector = new IPEndPoint(IPAddress.Parse(settingData.IPs), settingData.Port);
                    socket.Connect(detector);

                    util.LogFormat("SOCKET Connect Success");

                    int sendByte = socket.Send(StructToByte(pkt), Marshal.SizeOf(pkt), SocketFlags.None);
                    util.LogFormat("Send Packeage : " + sendByte);

                    DETECTOR_PKT receivePacket = new DETECTOR_PKT();
                    byte[] receiveData = new byte[Marshal.SizeOf(receivePacket)];
                    int ReceivedByte = socket.Receive(receiveData);
                    receivePacket = new DETECTOR_PKT(receiveData, Marshal.SizeOf(receivePacket));

                    util.LogFormat("Receive Packeage : " + ReceivedByte);

                    if (receivePacket.DID[0] == 'A'
                        && receivePacket.DID[1] == 'U'
                        && receivePacket.DID[2] == 'R'
                        && receivePacket.DID[3] == 'A')
                    {
                        util.LogFormat("OP CODE : " + Convert.ToString(receivePacket.OPCode, 16));
                    }
                }
                catch (Exception)
                {
                    util.LogFormat("SOCKET Connect Failed");
                }

                socket.Close();
            }
        }

        public string GetDarkImage()
        {
            byte[] did = new byte[4];
            did[0] = (byte)'A';
            did[1] = (byte)'U';
            did[2] = (byte)'R';
            did[3] = (byte)'A';

            ushort opcode = 0xaa05;
            ushort size = 0x0000;

            DETECTOR_PKT pkt = new DETECTOR_PKT(did, opcode, size);
            return SocketGetDarkMessage(pkt);
        }

        
        string SocketGetDarkMessage(DETECTOR_PKT pkt)
        {
            Socket socket = new Socket(SocketType.Stream, ProtocolType.Tcp);

            string ip = settingData.IPs;
            int port = settingData.Port;
            int timeout = settingData.Timeout;
            string path = settingData.ImageFilePath;
            string fileName = "";
            try
            {

                socket.ReceiveTimeout = timeout;
                IPEndPoint detector = new IPEndPoint(IPAddress.Parse(ip), port);
                socket.Connect(detector);
                util.LogFormat("SOCKET Connect Success");
                int sendByte = socket.Send(StructToByte(pkt), Marshal.SizeOf(pkt), SocketFlags.None);
                util.LogFormat("Send Package : " + sendByte);

                DETECTOR_PKT_IMAGE_PACKET receivePacket = new DETECTOR_PKT_IMAGE_PACKET();
                byte[] receiveData = new byte[Marshal.SizeOf(receivePacket)];
                int ReceivedByte = socket.Receive(receiveData);
                receivePacket = new DETECTOR_PKT_IMAGE_PACKET(receiveData, Marshal.SizeOf(receivePacket));

                util.LogFormat("Receive Packeage : " + ReceivedByte);

                if (receivePacket.DID[0] == 'A'
                    && receivePacket.DID[1] == 'U'
                    && receivePacket.DID[2] == 'R'
                    && receivePacket.DID[3] == 'A')
                {
                    util.LogFormat("OP CODE : " + Convert.ToString(receivePacket.OPCode, 16));
                    if (receivePacket.OPCode == 0x0000)
                    {
                        util.LogFormat("Detector Error - Return OPCODE : 0x0000");
                    }
                    else if (receivePacket.OPCode == 0x5550)
                    {
                        DateTime dateTime = DateTime.Now;
                        fileName = "dark_" + dateTime.ToString("yyyyMMddHHmmss") + ".raw";
                        string fullFileName = path + "\\" + fileName;
                        FileStream fileStream = new FileStream(fullFileName, FileMode.Create | FileMode.Append, FileAccess.Write);

                        int totalLen = 0;
                        while (totalLen < 18874372)
                        {
                            byte[] imageData = new byte[50000];
                            int receiveLen = socket.Receive(imageData);
                            if (receiveLen < 0)
                            {
                                util.LogFormat("ERROR : packet size wrong");
                            }


                            if (totalLen >= 18874372)
                            {
                                fileStream.Write(imageData, 0, receiveLen - 4);
                            }
                            else
                            {
                                fileStream.Write(imageData, 0, receiveLen);
                            }

                            totalLen += receiveLen;
                        }
                        fileStream.Close();
                    }
                }
            }
            catch (Exception)
            {
                util.LogFormat("SOCKET Connect Failed");
                return "";
            }

            socket.Close();
            return fileName;
        }

        public string GetBrightImage()
        {
            string ret = "retry";

            byte[] did = new byte[4];
            did[0] = (byte)'A';
            did[1] = (byte)'U';
            did[2] = (byte)'R';
            did[3] = (byte)'A';

            ushort opcode = 0xaa06;
            ushort size = 0x0000;

            DETECTOR_PKT pkt = new DETECTOR_PKT(did, opcode, size);

            bool automode = settingData.DetectorModeAuto;
            
            while (ret.Equals("retry"))
            {
                if (automode)
                {
                    ret = SocketGetBrightMessageAuto(pkt, 0);
                }
                else
                {
                    ret = SocketGetBrightMessage(pkt, 0);
                }
            }

            return ret;
        }

        public string GetAcquisitionImage()
        {
            string ret = "retry";

            byte[] did = new byte[4];
            did[0] = (byte)'A';
            did[1] = (byte)'U';
            did[2] = (byte)'R';
            did[3] = (byte)'A';

            ushort opcode = 0xaa06;
            ushort size = 0x0000;

            DETECTOR_PKT pkt = new DETECTOR_PKT(did, opcode, size);

            bool automode = settingData.DetectorModeAuto;

            while (ret.Equals("retry"))
            {
                if (automode)
                {
                    ret = SocketGetBrightMessageAuto(pkt, 1);
                }
                else
                {
                    ret = SocketGetBrightMessage(pkt, 1);
                }
            }

            return ret;
        }

        private string SocketGetBrightMessage(DETECTOR_PKT pkt, int type)
        {
            string ip = settingData.IPs;
            int port = settingData.Port;
            string fileName = "";

            Socket socket = new Socket(SocketType.Stream, ProtocolType.Tcp);

            try
            {
                socket.ReceiveTimeout = settingData.Timeout;
                IPEndPoint detector = new IPEndPoint(IPAddress.Parse(ip), port);
                socket.Connect(detector);
                util.LogFormat("SOCKET Connect Success");

                int sendByte = socket.Send(StructToByte(pkt), Marshal.SizeOf(pkt), SocketFlags.None);
                util.LogFormat("Send Packeage : " + sendByte);

                DETECTOR_PKT receivePacket = new DETECTOR_PKT();
                byte[] receiveData = new byte[Marshal.SizeOf(receivePacket)];
                int ReceivedByte = socket.Receive(receiveData);
                receivePacket = new DETECTOR_PKT(receiveData, Marshal.SizeOf(receivePacket));

                util.LogFormat("Receive Packeage : " + ReceivedByte);

                if (receivePacket.DID[0] == 'A'
                    && receivePacket.DID[1] == 'U'
                    && receivePacket.DID[2] == 'R'
                    && receivePacket.DID[3] == 'A')
                {
                    util.LogFormat("OP CODE : " + Convert.ToString(receivePacket.OPCode, 16));

                    if (receivePacket.OPCode == 0x0000)
                    {
                        util.LogFormat("Detector Error - Return OPCODE : 0x0000");
                    }
                    else if (receivePacket.OPCode == 0x552F)
                    {
                        util.LogFormat("Receive Cancel Signal - 0x552F");
                    }
                    else if (receivePacket.OPCode == 0x5521) // Prep Signal
                    {
                        socket.Receive(receiveData);
                        receivePacket = new DETECTOR_PKT(receiveData, Marshal.SizeOf(receivePacket));

                        if (receivePacket.OPCode == 0x0000)
                        {
                            util.LogFormat("Detector Error - Return OPCODE : 0x0000");
                        }
                        else if (receivePacket.OPCode == 0x552F)
                        {
                            util.LogFormat("Receive Cancel Signal - 0x552F");
                        }
                        else if (receivePacket.OPCode == 0x55E0) // Window Open Signal
                        {
                            util.LogFormat("Receive : Windows Open Signal");
                            byte[] did = new byte[4];
                            did[0] = (byte)'A';
                            did[1] = (byte)'U';
                            did[2] = (byte)'R';
                            did[3] = (byte)'A';

                            ushort opcode = 0xaaff;
                            ushort size = 0x0000;

                            DETECTOR_PKT ackMessage = new DETECTOR_PKT(did, opcode, size);
                            socket.Send(StructToByte(ackMessage), Marshal.SizeOf(ackMessage), SocketFlags.None);
                            util.LogFormat("Send : ACK Message");
                            socket.Receive(receiveData);
                            receivePacket = new DETECTOR_PKT(receiveData, Marshal.SizeOf(receivePacket));

                            if (receivePacket.OPCode == 0x0000)
                            {
                                util.LogFormat("Detector Error - Return OPCODE : 0x0000");
                            }
                            else if (receivePacket.OPCode == 0x552F)
                            {
                                util.LogFormat("Receive Cancel Signal - 0x552F");
                            }
                            else if (receivePacket.OPCode == 0x5522) // Exp Signal
                            {
                                util.LogFormat("Receive : Exposure Signal");
                                socket.Receive(receiveData);
                                receivePacket = new DETECTOR_PKT(receiveData, Marshal.SizeOf(receivePacket));

                                if (receivePacket.OPCode == 0x0000)
                                {
                                    util.LogFormat("Detector Error - Return OPCODE : 0x0000");
                                }
                                else if (receivePacket.OPCode == 0x552F)
                                {
                                    util.LogFormat("Receive Cancel Signal - 0x552F");
                                }
                                else if (receivePacket.OPCode == 0x55E1) // Report Window Close
                                {
                                    util.LogFormat("Receive : REPORT Window Closed Signal");
                                    socket.Send(StructToByte(ackMessage), Marshal.SizeOf(ackMessage), SocketFlags.None);
                                    util.LogFormat("Send : ACK Message");

                                    socket.Receive(receiveData);
                                    receivePacket = new DETECTOR_PKT(receiveData, Marshal.SizeOf(receivePacket));

                                    if (receivePacket.OPCode == 0x0000)
                                    {
                                        util.LogFormat("Detector Error - Return OPCODE : 0x0000");
                                    }
                                    else if (receivePacket.OPCode == 0x55E2) // Report Image Ready
                                    {
                                        util.LogFormat("Receive : REPORT Image Ready Signal");
                                        
                                        socket.Send(StructToByte(ackMessage), Marshal.SizeOf(ackMessage), SocketFlags.None);
                                        util.LogFormat("Send : ACK Message");

                                        DETECTOR_PKT_IMAGE_PACKET receiveAckPacket = new DETECTOR_PKT_IMAGE_PACKET();
                                        byte[] receiveAckData = new byte[Marshal.SizeOf(receiveAckPacket)];
                                        socket.Receive(receiveAckData);
                                        receiveAckPacket = new DETECTOR_PKT_IMAGE_PACKET(receiveAckData, Marshal.SizeOf(receiveAckPacket));


                                        if (receiveAckPacket.OPCode == 0x0000)
                                        {
                                            util.LogFormat("Detector Error - Return OPCODE : 0x0000");
                                        }
                                        else if (receiveAckPacket.OPCode == 0x5551)
                                        {
                                            DateTime dateTime = DateTime.Now;
                                            fileName = (type == 0 ? "Bright_" : "Acq_") + dateTime.ToString("yyyyMMddHHmmss") + ".raw";
                                            string fullFileName = settingData.ImageFilePath + "\\" + fileName;
                                            FileStream fileStream = new FileStream(fullFileName, FileMode.Create | FileMode.Append, FileAccess.Write);

                                            int totalLen = 0;
                                            while (totalLen < 18874372)
                                            {
                                                byte[] imageData = new byte[50000];
                                                int receiveLen = socket.Receive(imageData);
                                                if (receiveLen < 0)
                                                {
                                                    util.LogFormat("ERROR : packet size wrong");
                                                }


                                                if (totalLen >= 18874372)
                                                {
                                                    fileStream.Write(imageData, 0, receiveLen - 4);
                                                }
                                                else
                                                {
                                                    fileStream.Write(imageData, 0, receiveLen);
                                                }

                                                totalLen += receiveLen;
                                            }
                                            fileStream.Close();


                                            // Exam end Message
                                            opcode = 0xaa1e;
                                            size = 0x0000;

                                            DETECTOR_PKT examEnd = new DETECTOR_PKT(did, opcode, size);
                                            socket.Send(StructToByte(examEnd), Marshal.SizeOf(examEnd), SocketFlags.None);
                                            util.LogFormat("Send : Exam End Message");

                                            DETECTOR_PKT examEndAck = new DETECTOR_PKT();
                                            byte[] examEndAckData = new byte[Marshal.SizeOf(examEndAck)];
                                            socket.Receive(examEndAckData);
                                            examEndAck = new DETECTOR_PKT(examEndAckData, Marshal.SizeOf(examEndAck));

                                            if (examEndAck.OPCode == 0x0000)
                                            {
                                                util.LogFormat("Detector Error - Return OPCODE : 0x0000");
                                            }
                                            else if (examEndAck.OPCode == 0x55EC)
                                            {
                                                util.LogFormat("Receive : Report detector cleaning");
                                                // detector cleaning
                                                socket.Send(StructToByte(ackMessage), Marshal.SizeOf(ackMessage), SocketFlags.None);
                                                util.LogFormat("Send : ACK Message");

                                                socket.Receive(receiveData);
                                                receivePacket = new DETECTOR_PKT(receiveData, Marshal.SizeOf(receivePacket));

                                                if (examEndAck.OPCode == 0x0000)
                                                {
                                                    util.LogFormat("Detector Error - Return OPCODE : 0x0000");
                                                }
                                                else if (examEndAck.OPCode == 0x55EB)
                                                {
                                                    util.LogFormat("Receive : Report detector power saving");
                                                    socket.Send(StructToByte(ackMessage), Marshal.SizeOf(ackMessage), SocketFlags.None);
                                                    util.LogFormat("Send : ACK Message");

                                                    socket.Receive(receiveData);
                                                    receivePacket = new DETECTOR_PKT(receiveData, Marshal.SizeOf(receivePacket));
                                                    
                                                }
                                            }
                                        }
                                    }
                                    socket.Close();
                                }
                            }
                        }
                    }

                }
            }
            catch (Exception)
            {

                socket.Close();
                fileName = "";
            }

            return fileName;
        }

        private string SocketGetBrightMessageAuto(DETECTOR_PKT pkt, int type)
        {
            Socket socket = new Socket(SocketType.Stream, ProtocolType.Tcp);

            string ip = settingData.IPs;
            int port = settingData.Port;
            string fileName = "";

            try
            {
                socket.ReceiveTimeout = 5000; // timeout 5sec
                IPEndPoint detector = new IPEndPoint(IPAddress.Parse(ip), port);
                socket.Connect(detector);
                util.LogFormat("SOCKET Connect Success");

                int sendByte = socket.Send(StructToByte(pkt), Marshal.SizeOf(pkt), SocketFlags.None);
                util.LogFormat("Send Packeage : " + sendByte);

                DETECTOR_PKT receivePacket = new DETECTOR_PKT();
                byte[] receiveData = new byte[Marshal.SizeOf(receivePacket)];
                int ReceivedByte = socket.Receive(receiveData);
                receivePacket = new DETECTOR_PKT(receiveData, Marshal.SizeOf(receivePacket));

                util.LogFormat("Receive Packeage : " + ReceivedByte);
                
                if (receivePacket.DID[0] == 'A'
                    && receivePacket.DID[1] == 'U'
                    && receivePacket.DID[2] == 'R'
                    && receivePacket.DID[3] == 'A')
                {
                    util.LogFormat("OP CODE : " + Convert.ToString(receivePacket.OPCode, 16));
                    if (receivePacket.OPCode == 0x0000)
                    {
                        util.LogFormat("Detector Error - Return OPCODE : 0x0000");
                    }
                    else if (receivePacket.OPCode == 0x552F)
                    {
                        util.LogFormat("Receive Cancel Signal - 0x552F");
                    }
                    else if (receivePacket.OPCode == 0x55E0)
                    {
                        util.LogFormat("Receive : Windows Open Signal");
                        byte[] did = new byte[4];
                        did[0] = (byte)'A';
                        did[1] = (byte)'U';
                        did[2] = (byte)'R';
                        did[3] = (byte)'A';

                        ushort opcode = 0xaaff;
                        ushort size = 0x0000;

                        DETECTOR_PKT ackMessage = new DETECTOR_PKT(did, opcode, size);
                        socket.Send(StructToByte(ackMessage), Marshal.SizeOf(ackMessage), SocketFlags.None);
                        util.LogFormat("Send : ACK Message");

                        DETECTOR_PKT_IMAGE_PACKET receiveAckPacket = new DETECTOR_PKT_IMAGE_PACKET();
                        byte[] receiveAckData = new byte[Marshal.SizeOf(receiveAckPacket)];
                        socket.Receive(receiveAckData);
                        receiveAckPacket = new DETECTOR_PKT_IMAGE_PACKET(receiveAckData, Marshal.SizeOf(receiveAckPacket));

                        util.LogFormat("Receive Packeage : " + ReceivedByte);
                        util.LogFormat("OP CODE : " + Convert.ToString(receiveAckPacket.OPCode, 16));
                        if (receiveAckPacket.OPCode == 0x0000)
                        {
                            util.LogFormat("Detector Error - Return OPCODE : 0x0000");
                        }
                        else if (receiveAckPacket.OPCode == 0x552F)
                        {
                            util.LogFormat("Receive Cancel Signal - 0x552F");
                        }
                        else if (receiveAckPacket.OPCode == 0x55E1) // Report Window Close
                        {
                            util.LogFormat("Receive : REPORT Window Closed Signal");
                            socket.Send(StructToByte(ackMessage), Marshal.SizeOf(ackMessage), SocketFlags.None);
                            util.LogFormat("Send : ACK Message");

                            socket.Receive(receiveData);
                            receivePacket = new DETECTOR_PKT(receiveData, Marshal.SizeOf(receivePacket));

                            if (receivePacket.OPCode == 0x0000)
                            {
                                util.LogFormat("Detector Error - Return OPCODE : 0x0000");
                            }
                            else if (receivePacket.OPCode == 0x55E2) // Report Image Ready
                            {
                                util.LogFormat("Receive : REPORT Image Ready Signal");

                                socket.Send(StructToByte(ackMessage), Marshal.SizeOf(ackMessage), SocketFlags.None);
                                util.LogFormat("Send : ACK Message");

                                socket.Receive(receiveAckData);
                                receiveAckPacket = new DETECTOR_PKT_IMAGE_PACKET(receiveAckData, Marshal.SizeOf(receiveAckPacket));

                                if (receiveAckPacket.OPCode == 0x0000)
                                {
                                    util.LogFormat("Detector Error - Return OPCODE : 0x0000");
                                }
                                else if (receiveAckPacket.OPCode == 0x5551)
                                {

                                    DateTime dateTime = DateTime.Now;
                                    fileName = (type == 0 ? "Bright_" : "Acq_") + dateTime.ToString("yyyyMMddHHmmss") + ".raw";
                                    string fullFileName = settingData.ImageFilePath + "\\" + fileName;
                                    FileStream fileStream = new FileStream(fullFileName, FileMode.Create | FileMode.Append, FileAccess.Write);

                                    int totalLen = 0;
                                    while (totalLen < 18874372)
                                    {
                                        byte[] imageData = new byte[50000];
                                        int receiveLen = socket.Receive(imageData);
                                        if (receiveLen < 0)
                                        {
                                            util.LogFormat("ERROR : packet size wrong");
                                        }


                                        if (totalLen >= 18874372)
                                        {
                                            fileStream.Write(imageData, 0, receiveLen - 4);
                                        }
                                        else
                                        {
                                            fileStream.Write(imageData, 0, receiveLen);
                                        }

                                        totalLen += receiveLen;
                                    }
                                    fileStream.Close();


                                    // Exam end Message
                                    opcode = 0xaa1e;
                                    size = 0x0000;

                                    DETECTOR_PKT examEnd = new DETECTOR_PKT(did, opcode, size);
                                    socket.Send(StructToByte(examEnd), Marshal.SizeOf(examEnd), SocketFlags.None);

                                    DETECTOR_PKT examEndAck = new DETECTOR_PKT();
                                    byte[] examEndAckData = new byte[Marshal.SizeOf(examEndAck)];
                                    socket.Receive(examEndAckData);
                                    examEndAck = new DETECTOR_PKT(examEndAckData, Marshal.SizeOf(examEndAck));

                                    if (examEndAck.OPCode == 0x0000)
                                    {
                                        util.LogFormat("Detector Error - Return OPCODE : 0x0000");
                                    }
                                    else if (examEndAck.OPCode == 0x55EC)
                                    {
                                        util.LogFormat("Receive : Report detector cleaning");
                                        // detector cleaning
                                        socket.Send(StructToByte(ackMessage), Marshal.SizeOf(ackMessage), SocketFlags.None);
                                        util.LogFormat("Send : ACK Message");

                                        socket.Receive(receiveData);
                                        receivePacket = new DETECTOR_PKT(receiveData, Marshal.SizeOf(receivePacket));

                                    }
                                }

                            }
                        }
                        
                    }
                    else
                    {
                        util.LogFormat("Cannot find operation");
                    }
                    socket.Close();
                }
            }
            catch (Exception)
            {

                socket.Close();
                fileName = "";
            }

            return fileName;
        }


        // Structure 정보를 Byte Array로 변환하는 함수
        public static byte[] StructToByte(object obj)
        {
            int nSize = Marshal.SizeOf(obj);
            byte[] arr = new byte[nSize];
            IntPtr ptr = Marshal.AllocHGlobal(nSize);

            Marshal.StructureToPtr(obj, ptr, true);
            Marshal.Copy(ptr, arr, 0, nSize);
            Marshal.FreeHGlobal(ptr);
            return arr;
        }


        internal int CreateCalibration(int width, int height, 
            ushort[] darkImages, int darkCount, ushort[] brightImages, int brightCount)
        {
            double std_Margin = settingData.StdMargin;
            double target_Gain = settingData.TargetGain;
            double gain_Margin = settingData.GainMargin;
            double offset_Margin = settingData.OffsetMargin;
            double sur_Margin = settingData.SurrMargin;

            dark = new ushort[width * height];
            offset = new short[width * height];
            gain = new float[width * height];
            bpm = new ushort[width * height];

            unsafe
            {

                

                int ret = CreateCalibrationInfo(      width,
                                                        height,
                                                        darkImages,
                                                        darkCount,
                                                        brightImages,
                                                        brightCount,
                                                        std_Margin,
                                                        target_Gain,
                                                        gain_Margin,
                                                        offset_Margin,
                                                        sur_Margin,
                                                        dark,
                                                        offset,
                                                        gain,
                                                        bpm
                                                    );

                if (ret == -1) return - 1;

               
                //*/
                // save meanDark
             {
                string fileName = "Avgdark.raw";
                string fullFileName = settingData.CalibrationFilePath + "\\" + fileName;
                if (File.Exists(fullFileName)) File.Delete(fullFileName);
                FileStream fileStream = new FileStream(fullFileName, FileMode.Create, FileAccess.Write);
                var binFormatter = new BinaryFormatter();
                var mStream = new MemoryStream();
                binFormatter.Serialize(mStream, dark);
                fileStream.Write(mStream.ToArray(), 0, width * height * sizeof(ushort));

                fileStream.Close();
            }
            // save offsetMap
            {
                string fileName = "Offset.raw";
                string fullFileName = settingData.CalibrationFilePath + "\\" + fileName;
                if (File.Exists(fullFileName)) File.Delete(fullFileName);
                FileStream fileStream = new FileStream(fullFileName, FileMode.Create, FileAccess.Write);
                var binFormatter = new BinaryFormatter();
                var mStream = new MemoryStream();
                binFormatter.Serialize(mStream, offset);
                fileStream.Write(mStream.ToArray(), 0, width * height * sizeof(short));

                fileStream.Close();
            }
            // save gainMap
            {
                string fileName = "Gain.raw";
                string fullFileName = settingData.CalibrationFilePath + "\\" + fileName;
                if (File.Exists(fullFileName)) File.Delete(fullFileName);
                FileStream fileStream = new FileStream(fullFileName, FileMode.Create, FileAccess.Write);
                var binFormatter = new BinaryFormatter();
                var mStream = new MemoryStream();
                binFormatter.Serialize(mStream, gain);
                fileStream.Write(mStream.ToArray(), 0, width * height * sizeof(float));

                fileStream.Close();
            }
            // save badPixelMap
            {
                string fileName = "BPM.raw";
                string fullFileName = settingData.CalibrationFilePath + "\\" + fileName;
                if (File.Exists(fullFileName)) File.Delete(fullFileName);
                FileStream fileStream = new FileStream(fullFileName, FileMode.Create, FileAccess.Write);
                var binFormatter = new BinaryFormatter();
                var mStream = new MemoryStream();
                binFormatter.Serialize(mStream, bpm);
                fileStream.Write(mStream.ToArray(), 0, width * height * sizeof(ushort));

                fileStream.Close();
            }

            }
            return 0;
        }

        internal void Calibrate(string fileName)
        {
            int width = settingData.FrameWidth;
            int height = settingData.FrameHeight;
            int size = width * height;

            ushort[] image = new ushort[size];
            string fullFileName = settingData.ImageFilePath + "\\" + fileName;
            FileStream imageStream = new FileStream(fullFileName, FileMode.Open, FileAccess.Read);
            for (int i = 0; i < size; i++)
            {
                byte[] readPixel = new byte[2];
                imageStream.Read(readPixel, 0, 2);

                image[i] = BitConverter.ToUInt16(readPixel, 0);
            }
            imageStream.Close();

            if (dark == null)
            {
                ushort[] darkimage = new ushort[size];
                fullFileName = settingData.CalibrationFilePath + "\\Avgdark.raw";
                imageStream = new FileStream(fullFileName, FileMode.Open, FileAccess.Read);
                for (int i = 0; i < size; i++)
                {
                    byte[] readPixel = new byte[2];
                    imageStream.Read(readPixel, i * 2, 2);

                    darkimage[i] = BitConverter.ToUInt16(readPixel, 0);
                }
                imageStream.Close();
            }
            if (offset == null)
            {
                short[] offsetimage = new short[size];
                fullFileName = settingData.CalibrationFilePath + "\\Offset.raw";
                imageStream = new FileStream(fullFileName, FileMode.Open, FileAccess.Read);
                for (int i = 0; i < size; i++)
                {
                    byte[] readPixel = new byte[2];
                    imageStream.Read(readPixel, i * 2, 2);

                    offsetimage[i] = BitConverter.ToInt16(readPixel, 0);
                }
                imageStream.Close();
            }
            if (gain == null)
            {
                float[] gainMapimage = new float[size];
                fullFileName = settingData.CalibrationFilePath + "\\Gain.raw";
                imageStream = new FileStream(fullFileName, FileMode.Open, FileAccess.Read);
                for (int i = 0; i < size; i++)
                {
                    byte[] readPixel = new byte[sizeof(float)];
                    imageStream.Read(readPixel, i * sizeof(float), sizeof(float));

                    gainMapimage[i] = BitConverter.ToSingle(readPixel, 0);
                }
                imageStream.Close();
            }
            if (bpm == null)
            {
                ushort[] bpmimage = new ushort[size];
                fullFileName = settingData.CalibrationFilePath + "\\BPM.raw";
                imageStream = new FileStream(fullFileName, FileMode.Open, FileAccess.Read);
                for (int i = 0; i < size; i++)
                {
                    byte[] readPixel = new byte[2];
                    imageStream.Read(readPixel, i * 2, 2);

                    image[i] = BitConverter.ToUInt16(readPixel, 0);
                }
                imageStream.Close();
            }

            ushort[] result = new ushort[size];

            Calibrate(image, width, height, dark, offset, gain, bpm, settingData.Refsat_Value, result);

            // rotation
            ushort[] temp = new ushort[size];
            int rotationWidth = width;
            int rotationHeight = height;
            if (settingData.Rotation % 2 == 1)
            {
                rotationWidth = height;
                rotationHeight = width;
            }
            for (int h = 0; h < height; h++)
            {
                for (int w = 0; w < width; w++)
                {
                    int hh, ww;
                    switch (settingData.Rotation)
                    {
                        case 1:
                            hh = w;
                            ww = height - h - 1;
                            break;
                        case 2:
                            hh = height - h - 1;
                            ww = width - w - 1;
                            break;
                        case 3:
                            hh = width - w - 1;
                            ww = h;
                            break;
                        default:
                            hh = h;
                            ww = w;
                            break;

                    }
                    // flip
                    if (settingData.Flip == 1)
                        ww = rotationWidth - ww - 1;

                    // invert
                    if (settingData.Invert)
                        temp[hh * rotationWidth + ww] = (ushort)(ushort.MaxValue - result[h * width + w]);
                    else
                        temp[hh * rotationWidth + ww] = result[h * width + w];
                }
            }

            // clip
            int left = settingData.LeftCutEdge;
            int right = rotationWidth - settingData.RightCutEdge;
            int newWidth = right - left;
            int top = settingData.TopCutEdge;
            int bottom = rotationHeight - settingData.BottomCutEdge;
            int newHeight = bottom - top;

            ushort[] saveResult = new ushort[newWidth * newHeight];

            for (int h = top; h < bottom; h++)
            {
                for (int w = left; w < right; w++)
                {
                    int ww = w - left;
                    int hh = h - top;
                    saveResult[hh * newWidth + ww] = temp[h * rotationWidth + w];
                }

            }
           
            fullFileName = settingData.ImageFilePath + "\\" + fileName;
            if (File.Exists(fullFileName)) File.Delete(fullFileName);
            FileStream fileStream = new FileStream(fullFileName, FileMode.Create, FileAccess.Write);
            var binFormatter = new BinaryFormatter();
            var mStream = new MemoryStream();
            binFormatter.Serialize(mStream, saveResult);
            fileStream.Write(mStream.ToArray(), 0, newWidth * newHeight * sizeof(ushort));

            fileStream.Close();
        }
    }


}
